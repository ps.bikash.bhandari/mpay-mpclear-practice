#!/usr/bin/env bash
#Script created to launch k6 tests directly from the current terminal without accessing the k6 master pod.
#It requires that you supply the path to the JS file

namespace="k6"
jsfile="/home/amin/Documents/prepare_mpay_k8s/jompclear-benchmark/k6.io/k6_profile/B2P.js"

pod_path="/tmp/$(basename $jsfile)"

#Get K6 Master pod details

master_pod=`kubectl -n $namespace get po | grep k6 | awk '{print $1}'`

kubectl -n $namespace cp "$jsfile" "$master_pod:$pod_path"

## for k6 cloud
# kubectl -n $namespace exec -ti $master_pod -- k6 login cloud -t 4d1a437f249ebfd1a4a6abaf030c283626d95d3df912512d961fc2d582dbb7d4


## Echo Starting k6 load test Cloud
# kubectl -n $namespace exec -ti $master_pod -- k6 run --out cloud --include-system-env-vars=true "$pod_path"


## Echo Starting k6 load test Influx
kubectl -n $namespace exec -ti $master_pod -- k6 run --out influxdb=http://jmeter-loadtest-influxdb.jmeter:8086/k6 --include-system-env-vars=true "$pod_path"