helm upgrade \
--install --atomic --force --wait \
--timeout 9000 --namespace=k6 --tiller-namespace=kube-system \
--kubeconfig ~/.kube/mpay.conf \
-f k6io/values.yaml k6 ./k6io/