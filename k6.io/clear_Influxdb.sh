#!/usr/bin/env bash
namespace="jmeter"
echo "Clearing Influxdb K6 Database"

influxdb_pod=`kubectl -n $namespace get po | grep influxdb | awk '{print $1}'`

kubectl -n $namespace exec -ti $influxdb_pod -- influx -database 'k6' -execute 'DROP SERIES FROM /.*/'