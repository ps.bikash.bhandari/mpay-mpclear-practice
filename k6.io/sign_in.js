import http from 'k6/http';
//import {randomSeed} from "k6";

var count = 0;

function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

//// If you want to avoid having to type --vus 10 and --duration 30s all the time
//// this is global options

export let options = {
  //// virtual user
  vus: 1,
  duration: '10m',
  
  //// or duration as stage 1m30s
//   stages: [
//     { duration: '5s', target: 10, vus: 1 },
//     { duration: '5s', target: 10, vus: 10 },
//     { duration: '5s', target: 10, vus: 100 },
//   ],
};

var Coreurl = 'http://${__ENV.COREURL}/DINARAK/soap/process/operation?token=oURlX8ZSNA8ZsDd4i1OgGyHjnM9MlL3AfanMby3DkMg=';
var Springurl = 'http://${__ENV.SPRINGURL}/processMessage?token=oURlX8ZSNA8ZsDd4i1OgGyHjnM9MlL3AfanMby3DkMg=';


export default function() {

  //randomSeed(123456789);
  //let rnd = 'k6io'+Math.random();
  //let rnd = __VU;
  //let rnd = __ITER;



  var SigninPayload = JSON.stringify({
    "deviceId":"serv1",
    "extraData":[
       {
          "key":"pass",
          "value":"A6xnQhbz4Vx2HuGl4lXwZ5U2I8iziLRFnhP5eNfIRvQ="
       }
    ],
    "lang":1,
    "msgId":"3NewB2Pk6io-" + count + "-" + uuidv4(),
    "operation":"signin",
    "sender":"serv1",
    "senderType":"C",
    "tenant":"DINARAK"
 });

 var B2PPayload = JSON.stringify({
    "requestedId":"123",
    "channelId":"Channel1",
    "shopId":"Shop1",
    "tenant":"DINARAK",
    "operation":"b2p",
    "sender":"serv1",
    "senderType":"C",
    "deviceId":"serv1",
    "lang":1,
    "msgId":"3NewB2Pk6io-" + count + "-" + uuidv4(),
    "pin":"A6xnQhbz4Vx2HuGl4lXwZ5U2I8iziLRFnhP5eNfIRvQ=",
    "amount":"1",
    "receiver":"00962785881080",
    "receiverType":"M",
    "notes":""
 });

  var params = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // if (count == 0) {
  //   var options = {
  //       //// virtual user
  //       vus: 1,
  //       duration: '2s',
  //   };
  //   http.post(Coreurl, SigninPayload, params);
  //   count = count + 1;
  //   }
  
  http.post(Springurl, B2PPayload, params);
  count = count + 1;
}

export function setup() {
  var SigninPayload = JSON.stringify({
    "deviceId":"serv1",
    "extraData":[
       {
          "key":"pass",
          "value":"A6xnQhbz4Vx2HuGl4lXwZ5U2I8iziLRFnhP5eNfIRvQ="
       }
    ],
    "lang":1,
    "msgId":"3NewB2Pk6io-" + count + "-" + uuidv4(),
    "operation":"signin",
    "sender":"serv1",
    "senderType":"C",
    "tenant":"DINARAK"
 });

  http.post(Coreurl, SigninPayload, params);
}