#!/usr/bin/env bash
#Script created to launch k6 tests directly from the current terminal without accessing the k6 master pod.
#It requires that you supply the path to the JS file

namespace="k6"
jsfile="/home/amin/Documents/prepare_mpay_k8s/jompclear-benchmark/k6.io/sign_in.js"; cp $jsfile /tmp/loadtest.js

kubectl create configmap loadtest -n k6 --from-file=/tmp/loadtest.js --dry-run=client -o yaml | kubectl apply -f -

#Get K6 Master pod details

master_pod=`kubectl -n $namespace get po | grep k6 | awk '{print $1}'`

## Echo Starting k6 load test

kubectl -n $namespace exec -ti $master_pod -- k6 run --out influxdb=http://jmeter-loadtest-influxdb.jmeter:8086/k6 /tmp/loadtest.js