import http from 'k6/http';
import { uuidv4 } from '../k6_profile/core_function.js';

var count = 0;
var mobileStart=`${__ENV.mobStart}`; 


var natid=mobileStart;

function range () {
  mobileStart = parseInt(mobileStart,10) + 1;
  return mobileStart;
}

function rangeid () {
  natid += 1;
  return natid;
}

export let options = {
    //// virtual user
    vus: 1,
    rps: '2'
    //duration: '60s',
};

export default function() {
  var tenant = `${__ENV.tenant}`;
  range();

  
  const CustReg = '{"operation":"p2p","sender":"0097423345579","senderType":"M","deviceId":"3","lang":1,"msgId":"p2p-' + count + '-' + uuidv4() + '","tenant":"'+tenant+'","extraData":[{"key":"amnt", "value":"5"},{ "key":"rcvId","value":"00'+mobileStart+'" },{"key":"rcvType","value":"M"},{"key":"data", "value": "testdata"}]}';

  count = count + 1;
  var params = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  var Coreurl = 'http://'+`${__ENV.url}`+'/mpay/soap/process/operation';
  console.log(Coreurl);
  http.post(Coreurl, CustReg, params);
  count = count + 1;
}
/*export function setup() {
  var tenant = `${__ENV.tenant}`;
  const SignIn = '{"deviceId":"1","lang":1,"msgId":"11V10010mB2Pk6io-' + count + '-' + uuidv4() + '","operation":"signin2","pin":"","sender":"AGENTONE","senderType":"C","tenant":"'+tenant+'","extraData":[{"key":"pass","value":"123"}]}';
 var params = {
  headers: {
    'Content-Type': 'application/json',},
  };
  var Coreurl = 'http://'+`${__ENV.url}`+'/processMessage?token='+send_request(SignIn);
  http.post(Coreurl, SignIn, params);
  count = count + 1;
}*/
