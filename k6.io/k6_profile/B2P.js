import http from 'k6/http';
import { send_request } from '../k6_profile/token_generate.js'; 
import { send_requests } from '../k6_profile/token_generate_Fin.js';
import { uuidv4 } from '../k6_profile/core_function.js';

var count = 0;
var mobileStart=`${__ENV.mobStart}`; 

var natid=mobileStart;

function range () {
  mobileStart = parseInt(mobileStart,10) + 1;    
    return mobileStart;
}

function rangeid () {
  natid += 1;
  return natid;
}

export let options = {
  //// virtual user
  vus: 5,
  rps: '10'
  //duration: '60s',

};

export default function() {
  
 range();
   var params = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  var tenant = `${__ENV.tenant}`;
    var B2P = '{"amount":"2","channelId":"","deviceId":"Portal-NAF-TEST2","lang":1,"msgId":"Cust-' + count + '-' + uuidv4() + '","notes":"","operation":"b2p2","pin":"B2Jc2h7W2taqTPcMiZIHgSwri8meLwd0vDIem2VzETw=","receiver":"00'+ mobileStart +'","receiverType":"M","requestedId":"","sender":"NAF-TEST2","senderType":"C","shopId":"","tenant":"'+tenant+'"}';
    count = count + 1;
  params = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  Coreurl = 'http://'+`${__ENV.url}`+'/processMessage?token='+send_requests(B2P);
  http.post(Coreurl, B2P, params);
  
}
export function setup() {
  var tenant = `${__ENV.tenant}`;
  const SignIn = '{"deviceId":"Portal-NAF-TEST2","extraData":[{"key":"pass","value":"8+M7hMZL4cyNTMcKh+nAleTq4uhOVGQwho8zQqaAeis="}],"lang":1,"msgId":"Cust-' + count + '-' + uuidv4() + '","operation":"signin2","sender":"NAF-TEST2","senderType":"C","tenant":"'+tenant+'"}';
 var params = {
  headers: {
    'Content-Type': 'application/json',},
  };
  var Coreurl = 'http://'+`${__ENV.url}`+'/processMessage?token='+send_request(SignIn);
  http.post(Coreurl, SignIn, params);
  count = count + 1;
}