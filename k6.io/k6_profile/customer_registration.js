import http from 'k6/http';
import { uuidv4 } from '../k6_profile/core_function.js';
import { randomIntFromInterval } from '../k6_profile/random_MOb.js';
var count = 0;
var mobileStart=`${__ENV.mobStart}`; 

var INC_COUNT = 10000


var natid=mobileStart;

function range () {
  mobileStart = parseInt(mobileStart,10) + 1;
  return mobileStart;
}

function rangeid () {
  natid += 1;
  return natid;
}
export let options = {
    //// virtual user
    vus: 1,
    rps: '2'
    
    //duration: '60s',
};

export default function() {
  var tenant = `${__ENV.tenant}`;
  var randmob = ((INC_COUNT++) + "").substring(1)
  range();
  const CustReg = '{"deviceID":3,"lang":1,"msgId":"Cust-' + count + '-' + uuidv4() + '","operation":"cust","sender":"009740000'+ randmob + '","senderType":"M","tenant":"'+tenant+'","extraData":[{"key":"firstName","value":"bikash"},{"key":"midName","value":"test"},{"key":"lastName","value":"Mass"},{"key":"idTypeCode","value":"NIDN"},{"key":"idNumber","value":"009740000'+ randmob +'"},{"key":"dateOfBirth","value":"31/12/1983"},{"key":"countryCode","value":"QAT"},{"key":"cityCode","value":"1"},{"key":"address","value":"Amman Jordan - Mecca st"},{"key":"mobileNumber","value":"009740000'+ randmob +'"},{"key":"reference","value":"009740000'+ randmob +'"},{"key":"prefLanguage","value":"1"},{"key":"phone1","value":""},{"key":"email","value":"m@m.com"},{"key":"profileCode","value":"Default"},{"key":"gender","value":"2"},{"key":"notes","value":"Test customer"}]}';
  count = count + 1;
  var params = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  var Coreurl = 'http://'+`${__ENV.url}`+'/mpay/soap/process/operation';
  console.log(Coreurl);
  http.post(Coreurl, CustReg, params);
  count = count + 1;
}
/*export function setup() {
  var tenant = `${__ENV.tenant}`;
  const SignIn = '{"deviceId":"1","lang":1,"msgId":"11V10010mB2Pk6io-' + count + '-' + uuidv4() + '","operation":"signin2","pin":"","sender":"AGENTONE","senderType":"C","tenant":"'+tenant+'","extraData":[{"key":"pass","value":"123"}]}';
 var params = {
  headers: {
    'Content-Type': 'application/json',},
  };
  var Coreurl = 'http://'+`${__ENV.url}`+'/processMessage?token='+send_request(SignIn);
  http.post(Coreurl, SignIn, params);
  count = count + 1;
}*/
