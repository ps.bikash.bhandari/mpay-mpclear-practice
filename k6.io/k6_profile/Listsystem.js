import http from 'k6/http';
import { send_request } from '/home/bikash/gravity/jompclear-benchmark/k6.io/k6_profile/token_generate.js'; //
import { uuidv4 } from '../k6_profile/core_function.js';

var count = 0;

export let options = {
  //// virtual user
  vus: 1,
  duration: '2s',
};

export default function() {
  const ListsystemPayload = '{"deviceId":"1","sender":"00962797336668","operation":"sysconfig","lang":1,"tenant":"DINARAK","extraData":[{"key":"section","value":"All"}],"msgId":"V1010mB2P-' + count + '-' + uuidv4() + '","senderType":"M"}';
 
  var params = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  var Coreurl = 'http://'+`${__ENV.url}`+'/soap/process/operation?token='+send_request(ListsystemPayload);
  http.post(Coreurl, ListsystemPayload, params);
  count = count + 1;
}
