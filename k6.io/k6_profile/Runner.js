import http from "k6/http";
import { sleep, check } from "k6";
import CustRegistration from "../k6_profile/customer_registration.js";
import CashIn from "../k6_profile/cash_in.js";
import P2P from "../k6_profile/p2p.js";
import DoSalary from "../k6_profile/B2P.js";

var reg =  `${__ENV.Reg}`;
var cashin =  `${__ENV.CashIn}`;
var pertoper =  `${__ENV.P2P}`;
var salary =  `${__ENV.salary}`;

export default function() {
    if (reg == 1)
    {
    CustRegistration();
    
    }
    if (cashin == 1)
    {
      CashIn();
    
    }
    if (pertoper == 1)
    {
      P2P();
    
    }
    if (salary == 1)
    {
    DoSalary();        
    }
};


  //Run this command: k6 run -e tenant=DINARAK -e Reg=1 -e actreq=1 -e reqact=1 -e salary=1 -e url=10.99.155.11:7070 -e mobStart=962793123100 --iterations=2 Runner.js