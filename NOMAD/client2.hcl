log_level = "DEBUG"
data_dir = "/tmp/client2"
datacenter = "dc1"

# Enable raw_exec driver
plugin "raw_exec" {
  config {
    enabled = true
    no_cgroups = true
  }
}
client {
    enabled = true
    servers = ["127.0.0.1:4647"]
}

ports {
    http = 5657
}
