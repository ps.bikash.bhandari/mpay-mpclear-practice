job "mpay" {
  datacenters = ["dc1"]
  type = "service"

  group "tomcat" {
    count = 1
    restart {
      attempts = 3
      interval = "30m"
      delay = "60s"
      mode = "fail"
    }

    task "mpay" {
      driver = "raw_exec"
      resources {
        network {
          mbits = 10
          port "http" {static = 8080}
        }
      }

      env {
        CATALINA_OPTS="-Dport.http=$NOMAD_PORT_http -Ddefault.context=$NOMAD_TASK_DIR"
        JAVA_HOME="/usr/lib/jvm/jdk1.8.0_201"
        CATALINA_HOME="/usr/local/tomcat"
        CATALINA_OPTS="-Dport.http=$NOMAD_PORT_http -Ddefault.context=$NOMAD_TASK_DIR"
        CATALINA_HOME="/usr/local/tomcat"
        db_driver="oracle.jdbc.OracleDriver"
        db_type="ORACLE10"
        db_validation_query="SELECT 1 FROM DUAL"
        db_dialect="org.hibernate.dialect.Oracle10gDialect"
        db_check_query="select count(*) from user_tables"
        db_url="jdbc:oracle:thin:@192.168.100.245:1521:EE"
        db_user="MPAY"
        db_password="MPAY"
        db_schema="MPAY"
        env_broker_url="tcp://localhost:61616"
        env_broker_url_mpc="tcp://192.168.1.150:61999"

      }

      config {
        command = "/usr/local/tomcat/bin/catalina.sh"
        args = ["run"]
      }

      service {
        tags = ["urlprefix-/mpay"]
        port = "http"
        check {
          name     = "index_check"
          type     = "http"
          path     = "/mpay/jfw.html"
          interval = "10s"
          timeout  = "2s"
        }
}
    } # end task
  } # end group
} # end job
